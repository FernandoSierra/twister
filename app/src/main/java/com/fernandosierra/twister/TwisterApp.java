package com.fernandosierra.twister;

import android.app.Application;
import android.database.sqlite.SQLiteDatabase;

import com.fernandosierra.twister.data.database.DatabaseUtil;
import com.fernandosierra.twister.data.database.TwisterDatabaseHelper;
import com.fernandosierra.twister.internal.di.activity.ActivityComponentBuilder;
import com.fernandosierra.twister.internal.di.activity.HasActivitySubComponentBuilders;
import com.fernandosierra.twister.internal.di.app.AppComponent;
import com.fernandosierra.twister.internal.di.app.AppModule;
import com.fernandosierra.twister.internal.di.app.DaggerAppComponent;
import com.fernandosierra.twister.presentation.BaseActivity;

import java.util.Map;

import javax.inject.Inject;

public class TwisterApp extends Application implements HasActivitySubComponentBuilders {
    @Inject
    Map<Class<? extends BaseActivity>, ActivityComponentBuilder> activityComponentBuilders;
    @Inject
    TwisterDatabaseHelper databaseHelper;
    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
        appComponent.inject(this);

        SQLiteDatabase database = databaseHelper.getWritableDatabase();
        database.execSQL(DatabaseUtil.getEnableForeignKeys());
        database.close();
    }

    @Override
    public ActivityComponentBuilder getActivityComponentBuilder(Class<? extends BaseActivity> activityClass) {
        return activityComponentBuilders.get(activityClass);
    }
}
