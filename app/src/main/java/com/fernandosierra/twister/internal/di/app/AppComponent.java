package com.fernandosierra.twister.internal.di.app;

import com.fernandosierra.twister.TwisterApp;
import com.fernandosierra.twister.internal.di.activity.ActivityBindingModule;
import com.fernandosierra.twister.internal.di.scope.Application;

import dagger.Component;

@Application
@Component(modules = {AppModule.class, ActivityBindingModule.class})
public interface AppComponent {
    void inject(TwisterApp twisterApp);
}
