package com.fernandosierra.twister.internal.di.activity;

import com.fernandosierra.twister.presentation.main.MainActivity;
import com.fernandosierra.twister.presentation.main.MainActivityComponent;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module(subcomponents = {MainActivityComponent.class})
public interface ActivityBindingModule {

    @Binds
    @IntoMap
    @ActivityKey(MainActivity.class)
    ActivityComponentBuilder mainActivityComponentBuilder(MainActivityComponent.Builder builder);
}
