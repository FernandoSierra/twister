package com.fernandosierra.twister.internal.di.activity;

import com.fernandosierra.twister.presentation.BaseActivity;

import dagger.MembersInjector;

public interface ActivityComponent<T extends BaseActivity> extends MembersInjector<T> {
}
