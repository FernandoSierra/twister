package com.fernandosierra.twister.internal.di.activity;

import com.fernandosierra.twister.presentation.BaseActivity;

import dagger.MapKey;

@MapKey
public @interface ActivityKey {
    Class<? extends BaseActivity> value();
}
