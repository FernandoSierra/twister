package com.fernandosierra.twister.internal.di.app;

import android.content.Context;

import com.fernandosierra.twister.BuildConfig;
import com.fernandosierra.twister.data.GithubApi;
import com.fernandosierra.twister.data.database.TwisterDatabaseHelper;
import com.fernandosierra.twister.internal.di.scope.Application;

import dagger.Module;
import dagger.Provides;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.jackson.JacksonConverterFactory;

@Module
public class AppModule {
    private static final String AUTHORIZATION_HEADER = "Authorization";
    private static final String ACCEPT_HEADER = "Accept";
    private static final String OAUTH_TOKEN = "token 988ec0e5248841e1df5c1e02a67530523dd79891";
    private static final String ACCEPT_VALUE = "application/vnd.github.v3+json";
    private Context context;

    public AppModule(Context context) {
        this.context = context;
    }

    @Provides
    @Application
    Context provideContext() {
        return context;
    }

    @Provides
    @Application
    Interceptor provideInterceptor() {
        return chain -> {
            Request original = chain.request();
            Request request = original.newBuilder()
                    .header(AUTHORIZATION_HEADER, OAUTH_TOKEN)
                    .header(ACCEPT_HEADER, ACCEPT_VALUE)
                    .method(original.method(), original.body())
                    .build();
            return chain.proceed(request);
        };
    }

    @Provides
    @Application
    OkHttpClient provideOkHttpClient(Interceptor interceptor) {
        return new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .build();
    }

    @Provides
    @Application
    Retrofit provideRetrofit(OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(JacksonConverterFactory.create())
                .baseUrl(BuildConfig.API_URL)
                .client(okHttpClient)
                .build();
    }

    @Provides
    @Application
    TwisterDatabaseHelper provideTwisterDatabaseHelper(Context context) {
        return new TwisterDatabaseHelper(context);
    }

    @Provides
    @Application
    GithubApi provideGithubApi(Retrofit retrofit) {
        return retrofit.create(GithubApi.class);
    }
}
