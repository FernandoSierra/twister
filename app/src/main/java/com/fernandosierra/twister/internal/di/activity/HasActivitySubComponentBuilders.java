package com.fernandosierra.twister.internal.di.activity;

import com.fernandosierra.twister.presentation.BaseActivity;

public interface HasActivitySubComponentBuilders {
    ActivityComponentBuilder getActivityComponentBuilder(Class<? extends BaseActivity> activityClass);
}
