package com.fernandosierra.twister.internal.di.activity;

import com.fernandosierra.twister.internal.di.scope.Activity;
import com.fernandosierra.twister.presentation.BaseActivity;

import dagger.Module;
import dagger.Provides;

@Module
public abstract class ActivityModule<T extends BaseActivity> {
    protected final T activity;

    public ActivityModule(T activity) {
        this.activity = activity;
    }

    @Provides
    @Activity
    T provideActivity() {
        return activity;
    }
}
