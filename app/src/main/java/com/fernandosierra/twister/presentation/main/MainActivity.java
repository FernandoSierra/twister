package com.fernandosierra.twister.presentation.main;

import android.os.Bundle;

import com.fernandosierra.twister.R;
import com.fernandosierra.twister.internal.di.activity.HasActivitySubComponentBuilders;
import com.fernandosierra.twister.presentation.BaseActivity;

public class MainActivity extends BaseActivity<MainPresenter> implements MainView {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        presenter.init();
    }

    @Override
    protected void injectMembers(HasActivitySubComponentBuilders hasActivitySubComponentBuilders) {
        ((MainActivityComponent.Builder) hasActivitySubComponentBuilders.getActivityComponentBuilder(MainActivity.class))
                .activityModule(new MainActivityComponent.MainActivityModule(this))
                .build()
                .injectMembers(this);
    }
}
