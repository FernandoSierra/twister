package com.fernandosierra.twister.presentation;

import java.lang.ref.WeakReference;

public abstract class BasePresenter<T extends BaseView> {
    protected final WeakReference<T> view;

    public BasePresenter(T view) {
        this.view = new WeakReference<>(view);
    }
}
