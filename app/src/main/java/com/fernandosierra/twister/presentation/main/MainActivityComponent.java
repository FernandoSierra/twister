package com.fernandosierra.twister.presentation.main;

import com.fernandosierra.twister.data.GithubApi;
import com.fernandosierra.twister.internal.di.activity.ActivityComponent;
import com.fernandosierra.twister.internal.di.activity.ActivityComponentBuilder;
import com.fernandosierra.twister.internal.di.activity.ActivityModule;

import dagger.Module;
import dagger.Provides;
import dagger.Subcomponent;

@Subcomponent(modules = {MainActivityComponent.MainActivityModule.class})
public interface MainActivityComponent extends ActivityComponent<MainActivity> {

    @Subcomponent.Builder
    interface Builder extends ActivityComponentBuilder<MainActivityModule, MainActivityComponent> {
    }

    @Module
    class MainActivityModule extends ActivityModule<MainActivity> {
        MainActivityModule(MainActivity activity) {
            super(activity);
        }

        @Provides
        MainView provideMainView() {
            return activity;
        }

        @Provides
        MainPresenter provideMainPresenter(MainView mainView, GithubApi githubApi) {
            return new MainPresenter(mainView, githubApi);
        }
    }
}
