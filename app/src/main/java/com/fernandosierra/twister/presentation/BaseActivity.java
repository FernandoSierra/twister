package com.fernandosierra.twister.presentation;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.fernandosierra.twister.TwisterApp;
import com.fernandosierra.twister.internal.di.activity.HasActivitySubComponentBuilders;

import javax.inject.Inject;

public abstract class BaseActivity<T extends BasePresenter> extends AppCompatActivity {
    @Inject
    protected T presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupActivityComponents();
    }

    protected void setupActivityComponents() {
        injectMembers(((TwisterApp) getApplication()));
    }

    protected abstract void injectMembers(HasActivitySubComponentBuilders hasActivitySubComponentBuilders);
}
