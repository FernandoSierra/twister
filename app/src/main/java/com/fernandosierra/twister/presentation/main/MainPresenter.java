package com.fernandosierra.twister.presentation.main;

import android.util.Log;

import com.fernandosierra.twister.data.GithubApi;
import com.fernandosierra.twister.data.model.Repo;
import com.fernandosierra.twister.presentation.BasePresenter;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

class MainPresenter extends BasePresenter<MainView> {
    private final GithubApi githubApi;

    MainPresenter(MainView view, GithubApi githubApi) {
        super(view);
        this.githubApi = githubApi;
    }

    void init() {
        githubApi.getUserRepos()
                .subscribeOn(Schedulers.io())
                .flatMapObservable(Observable::fromIterable)
                .map(repo -> {
                    Log.d(MainPresenter.class.getSimpleName(), repo.toString());
                    return repo;
                })
                .toList()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new SingleObserver<List<Repo>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onSuccess(List<Repo> value) {
                        Log.d(MainActivity.class.getSimpleName(), "Repos: " + value.size());
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(MainActivity.class.getSimpleName(), e.getMessage(), e);
                    }
                });
    }
}
