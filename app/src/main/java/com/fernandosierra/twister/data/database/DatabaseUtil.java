package com.fernandosierra.twister.data.database;

import android.provider.BaseColumns;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.fernandosierra.twister.data.model.BaseModel;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

public class DatabaseUtil {
    private static final String CREATE_START = "CREATE TABLE IF NOT EXISTS ";
    private static final String COMMA = ",";
    private static final String NOT_NULL = "NOT NULL";
    private static final String TYPE_INTEGER = " INTEGER " + NOT_NULL;
    private static final String TYPE_TEXT = " TEXT " + NOT_NULL;
    private static final String TYPE_REAL = " REAL " + NOT_NULL;
    private static final String PRIMARY_KEY = " INTEGER PRIMARY KEY AUTOINCREMENT";
    private static final String ENABLE_FOREIGN_KEYS = "PRAGMA foreign_keys = ON";
    private static final String REFERENCES = " REFERENCES ";
    private static final String OPEN = "(";
    private static final String CLOSE = ")";
    private static final String FOREIGN_KEY = " FOREIGN KEY ";
    private static String foreignKeyStatement;

    private DatabaseUtil() {
        // Nothing
    }

    @NonNull
    public static String getCreateStatement(@NonNull Class<? extends BaseModel> clazz) {
        foreignKeyStatement = "";
        StringBuilder builder = new StringBuilder(CREATE_START);
        builder.append(clazz.getSimpleName());
        builder.append(OPEN);
        addPrimaryKey(builder);
        addAttributes(clazz, builder);
        builder.append(CLOSE);
        String createStatement = builder.toString();
        Log.d(DatabaseUtil.class.getSimpleName(), createStatement);
        return createStatement;
    }

    private static void addAttributes(@NonNull Class<? extends BaseModel> clazz, @NonNull StringBuilder builder) {
        for (Field field : clazz.getDeclaredFields()) {
            if (!Modifier.isStatic(field.getModifiers())) {
                boolean isAccessible = field.isAccessible();
                if (!isAccessible) {
                    field.setAccessible(true);
                }
                processField(builder, field);
                if (!isAccessible) {
                    field.setAccessible(false);
                }
            }
        }
        if (!foreignKeyStatement.isEmpty()) {
            builder.append(COMMA).append(foreignKeyStatement);
        }
    }

    private static void processField(@NonNull StringBuilder builder, @NonNull Field field) {
        Class<?> type = field.getType();
        String name = field.getName();
        String typeStatement;
        if (type.isPrimitive()) {
            typeStatement = processPrimitives(type);
        } else {
            typeStatement = processClass(name, type);
        }
        if (typeStatement != null) {
            builder.append(COMMA).append(name).append(typeStatement);
        }
    }

    @Nullable
    private static String processClass(@NonNull String name, @NonNull Class<?> type) {
        String typeStatement = null;
        if (type.equals(Integer.class) || type.equals(Long.class) || type.equals(Short.class) || type.equals(Boolean.class)) {
            typeStatement = TYPE_INTEGER;
        } else if (type.equals(Float.class) || type.equals(Double.class)) {
            typeStatement = TYPE_REAL;
        } else if (type.equals(String.class)) {
            typeStatement = TYPE_TEXT;
        } else if (type.getSuperclass().equals(BaseModel.class)) {
            typeStatement = TYPE_INTEGER;
            addForeignKey(name, type);
        }
        return typeStatement;
    }

    private static void addForeignKey(String name, Class<?> type) {
        foreignKeyStatement += FOREIGN_KEY + OPEN + name + CLOSE + REFERENCES + type.getSimpleName() + OPEN + BaseColumns._ID + CLOSE;
    }

    @Nullable
    private static String processPrimitives(@NonNull Class<?> type) {
        String typeStatement = null;
        if (type.equals(Integer.TYPE) || type.equals(Long.TYPE) || type.equals(Short.TYPE) || type.equals(Boolean.TYPE)) {
            typeStatement = TYPE_INTEGER;
        } else if (type.equals(Float.TYPE) || type.equals(Double.TYPE)) {
            typeStatement = TYPE_REAL;
        }
        return typeStatement;
    }

    private static void addPrimaryKey(@NonNull StringBuilder builder) {
        builder.append(BaseColumns._ID).append(PRIMARY_KEY);
    }

    @NonNull
    public static String getEnableForeignKeys() {
        return ENABLE_FOREIGN_KEYS;
    }
}
