package com.fernandosierra.twister.data.model;

import org.parceler.Parcel;

@Parcel
public class Owner extends BaseModel {
    private long id;

    @Override
    public String toString() {
        return "Owner{" +
                "id=" + id +
                '}';
    }
}
