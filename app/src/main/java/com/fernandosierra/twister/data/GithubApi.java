package com.fernandosierra.twister.data;

import com.fernandosierra.twister.data.model.Repo;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;

public interface GithubApi {
    String USER_REPOS = "user/repos";

    @GET(USER_REPOS)
    Single<List<Repo>> getUserRepos();
}
