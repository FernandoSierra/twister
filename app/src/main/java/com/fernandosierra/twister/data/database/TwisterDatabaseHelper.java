package com.fernandosierra.twister.data.database;


import android.content.Context;
import android.database.DefaultDatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.fernandosierra.twister.data.model.Owner;
import com.fernandosierra.twister.data.model.Repo;

public class TwisterDatabaseHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "Twister.sqlite";
    private static final int REPOS_VERSION = 1;
    private static final int CURRENT_VERSION = REPOS_VERSION;

    public TwisterDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, CURRENT_VERSION, new DefaultDatabaseErrorHandler());
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.beginTransaction();
        db.execSQL(DatabaseUtil.getCreateStatement(Owner.class));
        db.execSQL(DatabaseUtil.getCreateStatement(Repo.class));
        db.setTransactionSuccessful();
        db.endTransaction();
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Nothing
    }
}
