package com.fernandosierra.twister.data.database;


import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.fernandosierra.twister.BuildConfig;
import com.fernandosierra.twister.data.model.Repo;

public class RepoProvider extends ContentProvider {
    private static final int REPOS_ID = 100;
    private static final Uri CONTENT_URI = Uri.parse(BuildConfig.BASE_URI + Repo.class.getSimpleName());
    private static UriMatcher uriMatcher;
    private TwisterDatabaseHelper databaseHelper;

    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(BuildConfig.BASE_URI, Repo.class.getSimpleName(), REPOS_ID);
    }

    @Override
    public boolean onCreate() {
        databaseHelper = new TwisterDatabaseHelper(getContext());
        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        return null;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        if (uriMatcher.match(uri) == REPOS_ID) {
            SQLiteDatabase database = databaseHelper.getWritableDatabase();
            long insertedId = database.insert(Repo.class.getSimpleName(), null, values);
            database.close();
            if (insertedId > 0) {
                return Uri.fromParts(BuildConfig.BASE_URI, Repo.class.getSimpleName(), String.valueOf(insertedId));
            }
        }
        return null;
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        return 0;
    }
}
