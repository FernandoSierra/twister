package com.fernandosierra.twister.data.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import org.parceler.ParcelConstructor;

public class Repo extends BaseModel {
    private static final String FULL_NAME = "full_name";
    private static final String PRIVATE = "private";
    private long id;
    private String name;
    @JsonProperty(FULL_NAME)
    private String fullName;
    @JsonProperty(PRIVATE)
    private boolean privateRepo;
    private String url;
    private Owner owner;

    public Repo() {
    }

    @ParcelConstructor
    Repo(long id, String name, String fullName, boolean privateRepo, String url, Owner owner) {
        this.id = id;
        this.name = name;
        this.fullName = fullName;
        this.privateRepo = privateRepo;
        this.url = url;
        this.owner = owner;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public boolean isPrivateRepo() {
        return privateRepo;
    }

    public void setPrivateRepo(boolean privateRepo) {
        this.privateRepo = privateRepo;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Owner getOwner() {
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }

    @Override
    public String toString() {
        return "Repo{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", owner=" + owner +
                '}';
    }
}
